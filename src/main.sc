# require: dateTime/dateTime.sc
#     module = sys.zb-common
# require: functions.js
# require: patterns.sc
# require: patterns.sc
#     module = sys.zb-common
# require: scriptNew.js
#     type = scriptEs6
#     name = scriptNew

init:
    $global.$ = {
        __noSuchProperty__: function(property) {
            return $jsapi.context()[property];
        }
    };

patterns: 
    $good = [(очень/так*/конечно/всё/все/просто)] (хорош*/отличн*/классн*/крут*/замечательн*/потряс*/великолепн*/прекрасн*/прекрасно/заебись/зашибись/[о] кей/здорово/здорого/порядок/в порядке/отменн*/симпатичный/симпотн*)
    $package = (~пачка/банк*/баночк*/~коробка/коробочк*/обертк*/~пакет/пакетик*/~пленка/целлофан*/целофан*/целлафан*/целафан*/~бумага/~бумажный ~пакет/тетра пак*/тетрапак*)
    $myPattern = ( * {~оформить * (~упаковка/~стиль/{[~рождественский] ~елка})} *
        / * {(~делать/~сделать) * (~упаковка/{[~настоящий] ( ~произведение ~искусство )}/{[~необычный] ~дизайн}/{[~красивый] ~ленточка}/~рисунок/{[~фирменный/~красный] ~логотип})} *
        / * {~быть * (~упаковка/~магазин/~стиль/~бренд/~ребенок/~картинка/~интерьер/~путь/~заказ/~красивый/~привлекательный/~крепкий/~стильный)} *
        / * {~упаковывать * ({[~подарочный] ~коробк}/{[~яркий] ~цвет}/{[~необычный] ~форма}/{[~прозрачный] ~пакет}/~бантик/{[~красивый] ~бумага})} *
        / * {~упаковать * (~подарка/~заказ/( ~стиль ~винтаж )/~товар)} *
        / * {~добавить * (~упаковка/~украшение/{[~подарочный] ~элемент})} *
        / * {(~подойти/~смотреть) * ~упаковка} *
        / * {~подходить * (~упаковка/~интерьер/~настроение)} *
        / * {~выглядеть * (~упаковка/( ~часть ~подарок ))} *
        / * {не ~повредить * (~упаковка/~путь/~крепкий)} * 
        )

theme: /
    state: Start
        q!: * *start
        a: start
        script:
            $temp.a = new Array(5);
            $temp.b = _.range($temp.a.length);
        a: {{toPrettyString($temp.b)}}
        
            
        state: Catch
            event!: noMatch
            a: {{toPrettyString($parseTree.text)}}
        
        state: TEst
            q!: $myPattern
            # q!: * (~крем $honey/крем*/$honey) *
            a: test
            
        # state: TEsts
        #     q!: * $driedApricot *
        #     a: test2


    state: Silence || noContext = true
        event!: speechNotRecognized
        a: Простите, вас не слышно



    state: Feedback
        a: Подскажите, почему вы поставили такую оценку?
        script:
            $dialer.setNoInputTimeout(15000);
        
        state: GetFeedback
            event: noMatch
            a: Понятно, до свидания.
            script: $dialer.hangUp();

    state: NoInput || noContext = true
        event!: speechNotRecognized
        a: Повторите, пожалуйста, вас плохо слышно.

    
    state: AttemptsCheck
        script: $temp.dialHystory = $dialer.getDialHistory(); 
        if: ($temp.dialHystory.completedAttemptsToPhone >= $temp.dialHystory.availableAttemptsToPhone)
            a: Я больше не буду вам звонить
        else:
            a: Может быть еще созвонимся



    state: OnCallNotConnected 
        event: onCallNotConnected 
        script: 
            $dialer.setCallResult("Не дозвонился. Причина: " + $dialer.getCallNotConnectedReason()); 
            var now = new Date(); 
            $dialer.redial({ 
                startDateTime: new Date(now.getTime() + 60 * 60000), // Повторный звонок через час 
                maxAttempts: 2,                                      // 2 попытки дозвониться 
                retryIntervalInMinutes: 5                            // Пауза между попытками 5 минут 
            }); 
 

    state: CallbackInAnHour
        q!: * перезвони* * через час* *
        a: Хорошо! Я перезвоню вам через час.
        script:
            var now = new Date();
            $dialer.redial({
                startDateTime: new Date(now.getTime() + 60 * 60000),  // Повторный звонок через час
                finishDateTime: new Date(now.getTime() + 75 * 60000), // В течение 15 минут
                maxAttempts: 2,                                       // 2 попытки дозвониться
                retryIntervalInMinutes: 5                             // Пауза между попытками 5 минут
            });
            $dialer.hangUp();

    state: Question
        a: Вы согласны с предложением?
        script: $dialer.setCallResult("Запрашиваем решение");
        
        state: Agree
            q: * да *
            a: Рад это слышать! До свидания!
            script:
                $dialer.setCallResultAccepted();
                $dialer.hangUp();

        state: Disagree
            q: * нет *
            a: Очень жаль. До свидания!
            script:
                $dialer.setCallResultRejected();
                $dialer.hangUp();
        
        state: Fallback
            event: noMatch
            event: speechNotRecognized
            a: А уже и неважно! До свидания!
            script:
                $dialer.setCallResult("Решение неизвестно");
                $dialer.hangUp();


    state: Age
        a: Для продолжения подтвердите, пожалуйста, что являетесь совершеннолетним.
        
        state: Child
            q: * нет *
            a: К сожалению, данное предложение доступно только для совершеннолетних клиентов.
            a: Хорошего продолжения дня!
            script: 
                $dialer.reportData("Клиент совершеннолетний", "Нет");
                $dialer.hangUp();

            
        state: Adult
            q: * да *
            a: Отлично! Продолжаем!
            script: $dialer.reportData("Клиент совершеннолетний", "Да");
            go!: /NextContext


    state: Offer
        a: Новое предложение только для вас!
        a: Только сегодня и только у нас на все тарифы линейки «Живи» скидка 20%!
        a: Хотите воспользоваться нашим предложением?
        script:
            $dialer.bargeInResponse({
                bargeIn: "phrase", // Режим перебивания, в котором бот договорит фразу, прежде чем прерваться
                // bargeIn: "forced", // Режим перебивания, в котором бот прервет произношение фразы
                bargeInTrigger: "final", // Перебивание произойдет после окончательного результата распознавания
                // bargeInTrigger: "interim", // Перебивание произойдет с промежуточными результатами распознавания
                noInterruptTime: 1000 // В течение 1 секунды (1000 мс) после начала ответа бота нельзя перебить
            });

        state: What || noContext = true
            q: * что [что] *
            script:
                log($dialer.isBargeInInterrupted()); // => true
            a: Сегодня для вас персональная скидка — скидка 20% на все тарифы линейки «Живи»!
            a: Желаете попробовать?


    state: Offer
        a: Новое предложение только для вас! || bargeInTransition = /Offer/NotDone, bargeInLabel = firstReply
        a: На все тарифы линейки «Живи» скидка 20%! || ignoreBargeIn = true
        a: Хотите воспользоваться нашим предложением? || bargeInTransition = /Offer/Done, bargeInLabel = secondReply
        script: $dialer.bargeInResponse({ bargeIn: "forced", bargeInTrigger: "interim", noInterruptTime: 0 });
        go: /Offer/Done
    
        state: NotDone
    
            state: No
                intent: /Нет
                script: log($dialer.getBargeInTransition()); // => "/Offer/NotDone"
                a: Постойте, вы ведь еще не знаете, о чем речь! Это уникальное предложение!
                go!: /Offer

        state: Done
    
            state: No
                intent: /Нет
                a: Я поняла вас. Спасибо, что уделили мне время. Всего доброго!
                script: 
                    if ($dialer.isBargeInInterrupted()) {
                        log($dialer.getBargeInLabel()); // => "thirdReply"
                    }
                    $dialer.hangUp();


    state: Yes
        intent: /Да
        a: Спасибо! Опция будет добавлена в течение дня. Всего доброго, до свидания! || bargeInIf = beforeHangup
        script:
            $dialer.bargeInResponse({ bargeIn: "forced", bargeInTrigger: "final", noInterruptTime: 0 });
    
        state: BargeInIntent || noContext = true
            event: bargeInIntent
            script:
                var bargeInIntentStatus = $dialer.getBargeInIntentStatus();
                log(bargeInIntentStatus.bargeInIf); // => "beforeHangup"
                var text = bargeInIntentStatus.text;
    
                if (text.indexOf("оператор") > -1) {
                    $dialer.bargeInInterrupt(true);
                }
    
        state: Switch
            intent: /Оператор
            a: Прошу прощения, если я неправильно вас поняла. Перевожу ваш звонок на оператора.
            script: // тут перевод на оператора


    state: SaySmth
        audio: https//:storage.com/audioName.wav

    state: GenerateAudio
        script:
            $temp.priceConformed = $nlp.conform("рубль", $session.price);
            # Кэшируем аудио на неограниченное количество времени
            $imputer.cacheAudio("Example", {"price": $temp.priceConformed}), undefined, true }}

    state: SayFinalPrice
        # Воспроизводим кэшированную реплику
        audio: {{ $imputer.generateAudioUrl("Example", {"price": $temp.priceConformed}) }}


    state: GetCaller
        script: $temp.phone = $dialer.getCaller();
        a: Ваш номер телефона {{$temp.phone}}, верно?



    state: DtmfState
        script: 
            $response.replies = $response.replies || []; 
            $response.replies.push({ 
                type: 'dtmf', 
                max: 4,            //максимальное количество цифр, которое ожидается от абонента. 
                timeout: 15000     //интервал ожидания ввода от абонента в миллисекундах, число. 
            });
        
        state: GetDtmf
            q: *
            a: Принял

        state: noDTMF || noContext = true 
            event: noDtmfAnswerEvent 
            a: Вы ничего не выбрали. 


    state: clientHangup 
        event: hangup
        script: $client.lastActiveTime = currentDate(); 



    state: HangUp 
        a: Спасибо, всего доброго!
        script:$dialer.hangup("Бот повесил трубку"); 


    state: clientHangUp 
        event: hangup 
        event: botHangup 
        script: $client.lastActiveTime = currentDate(); 


    state: Hello 
        a: Здравствуйте! 
        if: $dialer.getPayload().name 
            a: Вас зовут {{$dialer.getPayload().name}} 
        else: 
            a: Я не знаю вашего имени. 



{ 
  "phone": "79990000000", 
  "name": "Иван", 
  "address": "Москва" 
}



    # state: Offer
    #     a: Новое предложение только для вас!
    #     a: Только сегодня и только у нас на все тарифы линейки «Живи» скидка 20%!
    #     a: Хотите воспользоваться нашим предложением?
    #     script:
    #         $dialer.bargeInResponse({
    #             bargeIn: "phrase", // Режим перебивания, в котором бот договорит фразу, прежде чем прерваться
    #             bargeInTrigger: "final", // Перебивание произойдет после окончательного результата распознавания
    #             noInterruptTime: 1000 // В течение 1 секунды после начала ответа бота нельзя перебить
    #         });

    #     state: What || noContext = true
    #         q: * что [что] *
    #         script:
    #             log($dialer.isBargeInInterrupted()); // => true
    #         a: Сегодня для вас персональная скидка — скидка 20% на все тарифы линейки «Живи»!
    #         a: Желаете попробовать?
                
                
        
    #         script:
    #             $dialer.bargeInResponse({
    #                 bargeIn: "phrase", // Режим перебивания, в котором бот договорит фразу, прежде чем прерваться
    #                 bargeIn: "forced", // Режим перебивания, в котором бот НЕ договорит фразу, а прервется

    #                 bargeInTrigger: "final", // Перебивание произойдет после окончательного результата распознавания
    #                 bargeInTrigger: "interim", // Перебивание произойдет с промежуточными результатами распознавания

    #                 noInterruptTime: 0 // Бота можно прервать сразу
    #                 noInterruptTime: 1000 // В течение 1 секунды после начала ответа бота нельзя перебить
    #             });
